#include "DecisionTree.hpp"
#include "util.hpp"

#ifndef RACER_RANDOMFOREST_HPP
#define RACER_RANDOMFOREST_HPP

class RandomForest {

public:
    vector<DecisionTree *> trees;
    float featureWeight;
    int depth;

    RandomForest(vector <vector<string>> &data, vector<int> &trainingIndx, vector <FeatureType> &featureType, int numTrees,
                 float baggingWeight, float featureWeight, int maxDepth);


    accuracyReport getAccuracy(vector <vector<string>>& datasetAsString,vector <int> &testIdxs);


    void printAccuracyReport(accuracyReport report);

    void printAccuracyReportFile(accuracyReport report);


};

string getForestPrediction(vector <string>& test, RandomForest *forest);

vector <string> getBatchPrediction(vector <vector<string>>& datasetAsString,vector <int>& testIdxs, RandomForest *forest);


#endif