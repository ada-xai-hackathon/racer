#include "helpers.hpp"
#include "DecisionTree.hpp"
#include "RandomForest.hpp"
#include "util.hpp"
#include <cmath>

using namespace std::chrono;
using namespace std;


int main(int argc, char *argv[]) {

    if (argc <= 1) {
        cout << "usage: ./racer <dataset name> <number of trees> <max depth> <feature selection weight>\n";
        exit(1);
    }


    //parse forest parameters
    const string dataset = argv[1]; //dataset name as found inside ../datasets directory
    int numTrees = atoi(argv[2]);
    float baggingWeight = 0.7;
    int depth = atoi(argv[3]);


    //parse dataset, feature types and feature names
    vector <vector<string>> datasetAsString;
    vector <FeatureType> featureTypes;
    vector <string> features;
    datasetAsString = parseDataToString("../datasets/" + dataset + ".data");
    featureTypes = parseFeatureTypes("../datasets/" + dataset + ".featureTypes");
    features = parseFeatures("../datasets/" + dataset + ".features");

    //pick number of features to select for random sub-spacing
    //if a third arg is provided we use that, if not we calculate an optimum weight based on number of features
    float featureWeight = argc < 5 ? sqrt(features.size()) / features.size() : atof(argv[4]);



    //separate testing and training data entries
    vector<int> trainingIdxs = randomSelect_WithoutReplacement(datasetAsString.at(0).size(), 0.7);
    vector<int> testingIdxs = splitTrainingAndTesting(trainingIdxs, datasetAsString);

    //oversample data to make sure all labels are equally represented
    cout << "Over sampling training data " << endl;
    vector<int> oversampledData = oversample(datasetAsString, trainingIdxs);
    trainingIdxs.insert(trainingIdxs.end(), oversampledData.begin(), oversampledData.end());


    //grow the forest
    RandomForest *randomForest = new RandomForest(datasetAsString, trainingIdxs, featureTypes, numTrees,
                                                  baggingWeight, featureWeight, depth);




    //get forest accuracy
    //accuracyReport report = randomForest->getAccuracy(datasetAsString, testingIdxs);

    //prints accuracy to console
    //randomForest->printAccuracyReport(report);

    //uncomment the following line to print accuracy into file accuracy-report.txt
//    randomForest->printAccuracyReportFile(report);


    //free up memory
    for (DecisionTree *tree : randomForest->trees) {
        cleanTree(tree->root);
        delete tree;
    }
    delete randomForest;


    return 0;

}
