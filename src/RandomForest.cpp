#include "RandomForest.hpp"
#include "DecisionTree.hpp"
#include "helpers.hpp"
#include <iterator>
#include <map>
#include <vector>
#include <typeinfo>
#include <pthread.h>


using namespace std::chrono;

//create a thread_pool for how many trees to train per number of threads chosen
vector<int> getParts(int trees, int cpus) {
    vector<int> parts;
    if (cpus > trees) {
        for (int i = 0; i < cpus; i++) {
            parts.push_back(1);
        }
    } else if (trees % cpus == 0) {
        for (int i = 0; i < cpus; i++)
            parts.push_back(trees / cpus);

    } else {
        // upto n-(x % n) the values
        // will be x / n
        // after that the values
        // will be x / n + 1
        int zp = cpus - (trees % cpus);
        int pp = trees / cpus;
        for (int i = 0; i < cpus; i++) {

            if (i >= zp)
                parts.push_back(pp + 1);
            else
                parts.push_back(pp);
        }
    }
    return parts;
}

//constructs <numTrees> number of trees with randomly chosen data entries
RandomForest::RandomForest(vector <vector<string>> &data, vector<int> &trainingIndx, vector <FeatureType> &featureTypes,
                           int numTrees,
                           float baggingWeight, float featureWeight, int maxDepth) {
    vector < DecisionTree * > decisionTrees;
    this->featureWeight = featureWeight;
    this->depth = maxDepth;

    //to concurrently train trees
    unsigned num_cpus = std::thread::hardware_concurrency()/2;
    if (numTrees < num_cpus)
        num_cpus = numTrees;
    //fix for VM issues
    if (num_cpus < 1)
        num_cpus = 1;

    // A mutex ensures orderly access.
    std::mutex iomutex;
    std::vector <std::thread> threads(num_cpus);
    std::atomic<int> treesTrained(1);

    cout << "Launching " << num_cpus << " jobs for training trees.\n";

    vector<int> temp = getParts(numTrees, num_cpus); //determine how many trees to run in parallel
    for (int i = 0; i < num_cpus; i++) {
        //start threads and train trees_per_thread number of trees in each
        if (i < temp.size())
            threads[i] = std::thread([&iomutex, i, temp, &data, &trainingIndx, baggingWeight,
                                             maxDepth, featureWeight, &featureTypes, &decisionTrees, &treesTrained] {
                for (int j = 0; j < temp.at(i); j++) {
                    //select random data entries for each tree
                    vector<int> bootstrappedData = bootstrapData(trainingIndx, baggingWeight);
                    int treeNumber = treesTrained;
                    treesTrained++;
                    cout << "Training tree " << treeNumber << " in thread " << i << endl;
                    //train a tree
                    DecisionTree *tree = new DecisionTree(data, bootstrappedData, maxDepth, featureWeight, featureTypes);
                    cout << "Done training tree " << treeNumber << " in thread " << i << endl;
                    {
                        // Use a lexical scope and lock_guard to safely lock the mutex only for
                        // the duration of vector push.
                        std::lock_guard <std::mutex> iolock(iomutex);
                        //add each trained tree to the forest
                        decisionTrees.push_back(tree);
                    }
                }


            });

    }
    for (auto &t : threads) {
        t.join();
    }

    this->trees = decisionTrees;

}

//run test data through all trees in forest
// return the label with maximum votes
string getForestPrediction(vector <string>& test, RandomForest *forest)  {
    //to save labels and vote counts
    map<string, int> votes;
    //get every tree's prediction
    for (int treeIdx = 0; treeIdx < forest->trees.size(); treeIdx++) {
        DecisionTree *tree = forest->trees[treeIdx];
        //get the prediction of the tree
        string prediction = tree->predictSingle(test, tree->root);
        if (votes.count(prediction)) {
            votes[prediction] += 1;

        } else {
            votes[prediction] = 1;

        }

    }

    //pick the biggest voted label
    int maxVote = 0;
    string label;
    map<string, int>::iterator itr;
    for (itr = votes.begin(); itr != votes.end(); ++itr) {
        if (maxVote < itr->second) {
            maxVote = itr->second;
            label = itr->first;
        }
    }


    return label;
}

//uses string RandomForest::getForestPrediction to predict for batch input
vector <string> getBatchPrediction(vector <vector<string>>& datasetAsString,vector <int>& testIdxs, RandomForest *forest) {

    vector <string> predictions;

    for (int testIndex = 0; testIndex < testIdxs.size(); testIndex++) {
        map<string, int> votes;
        //collect each column of the picked data entry
        vector <string> test;

        string emptystring;
        for (int featIndex = 0; featIndex < datasetAsString.size(); featIndex++) {
            test.push_back(emptystring);
        }

        for (int featIndex = 0; featIndex < datasetAsString.size(); featIndex++) {
            test.at(featIndex) = datasetAsString.at(featIndex).at(testIdxs[testIndex]);
        }


        //Get every tree in the forests prediction
        string label = getForestPrediction(test, forest);

        predictions.push_back(label);
    }

    return predictions;

}

//compares labels predicted with actual labels and saves report in accuracy report (struct defined in util.hpp)
accuracyReport RandomForest::getAccuracy(vector <vector<string>>& datasetAsString,vector <int> & testIdxs) {
    vector <string> predictions = getBatchPrediction(datasetAsString,testIdxs, this);
    vector <string> labels;
    for(int dataIdx:testIdxs){
        labels.push_back(datasetAsString.at(datasetAsString.size()-1).at(dataIdx));
    }

    std::map<std::string, int> incorrectlabels;
    std::map<std::string, int> correctlabels;

    float accuracy;
    int correct = 0;
    int incorrect = 0;
    int total = predictions.size();

    if (predictions.size() == labels.size()) {
        for (int itr = 0; itr < total; itr++) {
            if (predictions[itr] == labels[itr]) {
                correct += 1;
                if (correctlabels.count(labels[itr])) {
                    correctlabels[labels[itr]] += 1;
                } else {
                    correctlabels[labels[itr]] = 1;
                }
            } else {
                incorrect += 1;
                if (incorrectlabels.count(labels[itr])) {
                    incorrectlabels[labels[itr]] += 1;
                } else {
                    incorrectlabels[labels[itr]] = 1;
                }

            }

        }

        accuracy = ((float) correct / (float) total) * 100;
    } else {
        cerr << "Predictions and labels are not equal" << endl;
    }

    accuracyReport report = {accuracy, correctlabels, incorrectlabels, correct, incorrect, total};
    return report;


}

//print accuracy on console
void RandomForest::printAccuracyReport(accuracyReport report) {
    cout << "********************* Forest accuracy *****************" << endl;
    cout << "Testing accuracy for forest with " << this->trees.size() << " trees depth " << this->depth
         << " and feature selection weight " << this->featureWeight << endl;
    cout << "Total tested data is " << report.total << endl;
    cout << "The accuracy of the tree is " << report.accuracy << "% ";
    cout << " with " << report.correct << " correct predictions ";
    cout << " and " << report.incorrect << " incorrect predictions " << endl;
    map<string, int>::iterator citr;
    for (citr = report.correctlabels.begin(); citr != report.correctlabels.end(); ++citr) {
        cout << "label " << citr->first << " was predicted right " << citr->second << " times \n";
    }

    map<string, int>::iterator itr;
    for (itr = report.incorrectlabels.begin(); itr != report.incorrectlabels.end(); ++itr) {
        cout << "label " << itr->first << " was predicted wrong " << itr->second << " times \n";
    }

}

//saves accuracy into a file accuracy-report.txt
void RandomForest::printAccuracyReportFile(accuracyReport report) {
    cout << "Printing accuracy into accuracy-report.txt ..." << endl;
    ofstream outfile;
    outfile.open("accuracy-report.txt", ios::app);
    outfile << "---------- Report--------------" << "\n";
    outfile << "Testing accuracy for forest with " << this->trees.size() << " trees depth " << this->depth
            << " and feature selection weight " << this->featureWeight << endl;
    outfile << "Total tested data is " << report.total << endl;
    outfile << "The accuracy of the tree is " << report.accuracy << "% ";
    outfile << " with " << report.correct << " correct predictions ";
    outfile << " and " << report.incorrect << " incorrect predictions" << endl;

    map<string, int>::iterator citr;
    for (citr = report.correctlabels.begin(); citr != report.correctlabels.end(); ++citr) {
        outfile << "label " << citr->first << " was predicted right " << citr->second << " times \n";
    }

    map<string, int>::iterator itr;
    for (itr = report.incorrectlabels.begin(); itr != report.incorrectlabels.end(); ++itr) {
        outfile << "label " << itr->first << " was predicted wrong " << itr->second << " times \n";
    }
    outfile.close();
}


