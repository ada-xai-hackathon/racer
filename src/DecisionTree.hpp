#include "Node.hpp"
#include <string>
#include <vector>
#include "helpers.hpp"



#ifndef RACER_DECISIONTREE_HPP
#define RACER_DECISIONTREE_HPP


class DecisionTree {
public:
    DecisionTree(vector <vector<string>> &data, vector<int> &trainingIndx, int maxDepth, float featureWeight, vector <FeatureType> &featureType);

    string predictSingle(vector <string>& test, Node *treeRoot);

    void printTree(Node *node, int space);

    Node *root;
    int maxDepth;
    float featureWeight;


private:


};

Node *train(vector <vector<string>> &data, vector <FeatureType> &featureType,
            float parentEntropy, int currentDepth, int maxDepth, float featureWeight,  vector<int> nodeDatasetIndices );


#endif //RACER_DECISIONTREE_HPP
