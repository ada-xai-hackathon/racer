#include "Question.hpp"
#include "util.hpp"

Question::Question(int splitFeatureIndex, string splitValue, FeatureType splitFeatureType) {

    this->splitFeatureIndex = splitFeatureIndex;
    this->splitValue = splitValue;
    this->splitFeatureType = splitFeatureType;
}