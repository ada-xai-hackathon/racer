#include "DecisionTree.hpp"
#include "helpers.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include "Node.hpp"
#include "Question.hpp"

using namespace std;

DecisionTree::DecisionTree(vector <vector<string>> &data, vector<int> &trainingIndx, int maxDepth, float featureWeight,
                           vector <FeatureType> &featureType) {

    this->maxDepth = maxDepth;
    this->featureWeight = featureWeight;

    //contains pointer to the head of the tree
    this->root = train(data, featureType, 0.0, 0, maxDepth, featureWeight, trainingIndx);
}

Node *train(vector <vector<string>> &data, vector <FeatureType> &featureType,
            float parentEntropy, int currentDepth, int maxDepth, float featureWeight, vector<int> nodeDatasetIndices) {

    //get classification and entropy of current data
    std::pair<string, float> classificationAndEntropy = classifyWithEntropy(data, nodeDatasetIndices);
    string classification = classificationAndEntropy.first;
    float originalEntropy = classificationAndEntropy.second;

    //if depth exceeds the max depth or the data is pure make a leaf
    if (currentDepth > maxDepth || originalEntropy == 0.0) {
        Node *leaf = new Node(NULL, NULL, NULL, true, classification, originalEntropy);
        return leaf;

    } else {

        //create a random subspace
        //find possible splits
        //find best split
        BestSplitPoint bestSplit = findBestSplit(parentEntropy, currentDepth, data,
                                                 featureType, featureWeight, nodeDatasetIndices);


        //if we can't find a split point that's good enough we make a leaf
        if (bestSplit.featureIdx == -1 || bestSplit.featureIdx > data.size() - 1) {
            Node *leaf = new Node(NULL, NULL, NULL, true, classification, originalEntropy);
            return leaf;
        }

        //split data
        FeatureSplitDataIndx featureSplitData = splitData(data, bestSplit.featureIdx, featureType,
                                                          bestSplit.splitpoint, nodeDatasetIndices);



        //No longer splittable because data size is too small
        if (featureSplitData.leftSideData.size() < 1 || featureSplitData.rightSideData.size() < 1) {
            Node *leaf = new Node(NULL, NULL, NULL, true, classification, originalEntropy);
            return leaf;
        }

        //if we haven't made a leaf this far we make a node
        Question *question = new Question(bestSplit.featureIdx, bestSplit.splitpoint,
                                          featureType[bestSplit.featureIdx]);


        //call train for left and right data
        Node *leftNode = train(data, featureType, originalEntropy, currentDepth + 1, maxDepth,
                               featureWeight, featureSplitData.leftSideData);
        Node *rightNode = train(data, featureType, originalEntropy, currentDepth + 1, maxDepth,
                                featureWeight, featureSplitData.rightSideData);


        Node *node = new Node(question, leftNode, rightNode, false, classification, originalEntropy);

        return node;
    }
}


string DecisionTree::predictSingle(vector <string>& test, Node *treeRoot) {

    //base case: if a leaf is reached return classification
    if (treeRoot->isLeaf == true) {
        return treeRoot->classification;
    }
    Question *question = treeRoot->question;

    //get split value and index for current node
    int splitIndex = question->splitFeatureIndex;
    string splitValue = question->splitValue;
    FeatureType featureType = question->splitFeatureType;

    Node *answer;
    //chose which node to jump to according to test data
    if (featureType == CATEGORICAL) {
        if (test[splitIndex] == splitValue) {
            answer = treeRoot->leftBranch;
        } else {
            answer = treeRoot->rightBranch;
        }
    } else {
        if (stod(test[splitIndex]) <= stod(splitValue)) {
            answer = treeRoot->leftBranch;
        } else {
            answer = treeRoot->rightBranch;
        }
    }
    return predictSingle(test, answer);
}

void DecisionTree::printTree(Node *node, int space) {
    if (node == NULL) {
        return;
    }

    space += 10;
    if (!node->isLeaf) {
        this->printTree(node->rightBranch, space);
        cout << endl;
        for (int i = 10; i < space; i++)
            cout << " ";
        cout << node->question->splitFeatureIndex << " " << node->classification << " ";
        this->printTree(node->leftBranch, space);

    } else {
        cout << " " << node->classification << "\n";
    }

}
