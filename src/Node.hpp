#include "Question.hpp"
#include <string>
#ifndef RACER_NODE_H
#define RACER_NODE_H

class Node {
public:
    Node(Question *question, Node *leftBranch, Node *rightBranch, bool isLeaf, std::string classification,
         float originalEntropy);


    Question *question;
    Node *leftBranch;
    Node *rightBranch;
    bool isLeaf;
    string classification;
    float originalEntropy;
};

#endif //RACER_NODE_H
