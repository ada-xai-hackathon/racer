#!/usr/bin/python3

import numpy as np
import sys

# Parse dataset
with open(sys.argv[1], 'r') as inFile:
	data = [[float(value) for value in line.split(',')] for line in inFile]
M = np.array(data)

#	Example: M = M[(M[:,0] > 1.1] will filter out
#		every data point except where the first feature
#		is > 1.1.

# Filter Feature Number 0
M = M[ (M[:,0] > 0) ]
# Filter Feature Number 1
M = M[ (M[:,1] > -1) ]
# Filter Feature Number 2
M = M[ (M[:,2] < 5) ]
# Etc...

# If helpful, print the remaining entries
print(M)

# Count remaining classes after filter
classes = dict()
for m in M[:,-1]:
	thisClass = str(int(m))
	if thisClass in classes:
		classes[thisClass] = classes.get(thisClass) + 1
	else:
		classes[thisClass] = 1

#Print class counts
print(classes)
